*! runmixreglsstore.ado, George Leckie, 29May2014
****************************************************************************
* -runmixregls store-
****************************************************************************
program runmixreglsstore, eclass
	syntax, [First Second Third STORE(string)]

	* Parse stage options: first, second, and third	
	local temp = ("`first'"=="first") + ("`second'"=="second") + ("`third'"=="third")
	if (`temp'==0) {
		display as error "one of first, second, or third should be specified"
		exit 198
	}
	if (`temp'==2 | `temp'==3) {
		display as error "only one of first, second, or third should be specified"
		exit 198
	}
	if ("`first'"=="first")   local s = 1
	if ("`second'"=="second") local s = 2
	if ("`third'"=="third")   local s = 3	

	* Store active results
	tempname m
	estimates store `m'



	*-------------------------------------------------------------------------------
	* COPY ALL RESULTS
	*-------------------------------------------------------------------------------
	
	**************************************
	* ERETURN SCALARS
	**************************************
	local k          = e(k_`s')
	local time       = e(time)
	local ll         = e(ll_`s')
	local deviance   = e(deviance_`s')
	local iterations = e(iterations_`s')



	*-------------------------------------------------------------------------------
	* POST STAGE S RESULTS
	*-------------------------------------------------------------------------------
	
	* Post stage `s' results
	matrix b_`s' = e(b_`s')
	matrix V_`s' = e(V_`s')
	ereturn post b_`s' V_`s'
	ereturn local cmd "runmixregls"

	**************************************
	* ERETURN SCALARS
	**************************************

	ereturn scalar k          = `k'
	ereturn scalar time       = `time'
	ereturn scalar ll         = `ll'
	ereturn scalar deviance   = `deviance'
	ereturn scalar iterations = `iterations'



	*-------------------------------------------------------------------------------
	* DISPLAY STAGE S RESULTS
	*-------------------------------------------------------------------------------
	* Display stage `s' results
	display
	if (`s'==1) display as txt "First stage results"
	if (`s'==2) display as txt "Second stage results"
	if (`s'==3) display as txt "Third stage results"
	ereturn display

	
	*-------------------------------------------------------------------------------
	* STORE STAGE S RESULTS
	*-------------------------------------------------------------------------------
	* Store stage `s' results
	if ("`store'"!="") {
		estimates store `store'
	}
	quietly estimates restore `m'
end



********************************************************************************
exit
