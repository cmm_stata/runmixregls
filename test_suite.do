clear *
discard
set trace off
****************************************************************************

****************************************************************************
* Load the data Reisby
****************************************************************************

* Load the Reisby data into memory
use http://www.bristol.ac.uk/cmm/media/runmixregls/reisby, clear

recode hamdep (-9=.)

xtset id


********************************************************************************
* Use different stage results! 
********************************************************************************

runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog) ///
   iterate(1)

estimates store m0

runmixreglsstore, first store(m1)
runmixreglsstore, second store(m2)
runmixreglsstore, third store(m3)


estimates dir
estimates replay m0
estimates replay m1
estimates replay m2
estimates replay m3



 
****************************************************************************
* nocons
****************************************************************************

runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog) ///
   iterate(1)

gen one = 1

runmixregls hamdep one week endog endweek, nocons ///
   between(one endog, nocons) ///
   within(one week endog, nocons) ///
   iterate(1)


runmixregls hamdep one, nocons ///
   between(one, nocons) ///
   within(one, nocons) ///
   iterate(1)


 
   
****************************************************************************
* Import sampling covariances between theta1 and theta2
****************************************************************************
// doesn't currently work
runmixregls hamdep, iterate(1)


****************************************************************************
* factor notation
****************************************************************************
// not implemented


****************************************************************************
* level(99), coeflegend, cformat(%9.2f)
****************************************************************************
runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog) 
   
runmixregls, level(99)

runmixregls, coeflegend

runmixregls, cformat(%9.2f)



****************************************************************************
* noadapt
****************************************************************************

runmixregls hamdep, noadapt

runmixregls hamdep


****************************************************************************
* Intpoints
****************************************************************************

runmixregls hamdep, intpoints(2)

runmixregls hamdep, intpoints(4)


****************************************************************************
* Null effects model
****************************************************************************

runmixregls hamdep


****************************************************************************
* association
****************************************************************************

runmixregls hamdep, association(none)

runmixregls hamdep, association(quadratic)



****************************************************************************
* mean function covariates only
****************************************************************************

runmixregls hamdep week endog endweek



****************************************************************************
* BS variance covariates only
****************************************************************************

runmixregls hamdep week endog endweek, ///
   between(endog)


****************************************************************************
* WS variance covariates only
****************************************************************************

runmixregls hamdep one, nocons ///
   within(week endog)


****************************************************************************
* Full model
****************************************************************************

runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog)


****************************************************************************
* random effects and residuals
****************************************************************************

runmixregls hamdep, reffects(u v)

runmixregls hamdep, residuals(e)


****************************************************************************
* Predictions excluding random effects
****************************************************************************

runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog) ///
   reffects(theta1 theta2)

predict xb_m, equation(Mean)

predict xb_b, equation(Between)

predict xb_w, equation(Within)

predictnl sigma2_u = exp(xb(Between))

predictnl sigma2_e_pm = exp(xb(Within))

predictnl sigma2_e_pa = exp(xb(Within) + 0.5*[Scale]_b[sigma]^2)

predictnl sigma2_e_j = exp(xb(Within) + [Association]_b[linear]*theta1 + theta2)



****************************************************************************
* header
****************************************************************************

runmixregls hamdep, noheader


****************************************************************************
* if
****************************************************************************

egen subject = group(id)

runmixregls hamdep week endog endweek if subject<=55, ///
   between(endog) ///
   within(week endog)




****************************************************************************
* in
****************************************************************************

runmixregls hamdep week endog endweek in 1/300, ///
   between(endog) ///
   within(week endog)

// Correctly gives an error message
/*
runmixregls hamdep week endog endweek in 1/10, ///
   between(endog) ///
   within(week endog)
*/

****************************************************************************
* missing values
****************************************************************************

set seed 23525

gen u1 = runiform()
gen u2 = runiform()
gen u3 = runiform()

replace week    = . if u1<0.1
replace endog   = . if u2<0.1
replace endweek = . if u3<0.1

count if hamdep<. & week<. & endog<. & endweek<.

runmixregls hamdep week, ///
   between(endog) ///
   within(endweek)



****************************************************************************
* Saving files
****************************************************************************

runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog) 


****************************************************************************
* error message
****************************************************************************

// Correctly gives an error message
/*
runmixregls hamdep week endog endweek in 1/1, ///
   between(endog) ///
   within(week endog) ///
   iterate(3) 

keep in 1
drop if _n==1
runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog) ///
   iterate(3) 
*/   
   
   


****************************************************************************
* variable names same first 8 chars
****************************************************************************

gen var12345a123456789 = endog

gen var12345b123456789 = endweek

runmixregls hamdep week var12345a var12345b, ///
   between(var12345a) ///
   within(week var12345a) ///
   iterate(1) typedeffile typeoutfile

drop var12345a123456789 var12345b123456789


****************************************************************************
* long variable names
****************************************************************************

gen hamdep123456789 = hamdep

gen week123456789 = week

runmixregls hamdep123456789 week123456789 endog endweek, ///
   between(endog) ///
   within(week endog) ///
   iterate(1) typedeffile typeoutfile

drop hamdep123456789 week123456789



****************************************************************************
* Profiler
****************************************************************************

profiler clear
profiler on
runmixregls hamdep week endog endweek, ///
   between(endog) ///
   within(week endog)
profiler off
profiler report



****************************************************************************
exit
