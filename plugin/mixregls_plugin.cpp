#include "stplugin.h"
#include <algorithm>
#include <vector>
#include <string>
#include <future>
#include <chrono>
#include <thread>
#include <climits>
#include <cmath>

#ifndef _MSC_VER
#include <dlfcn.h>
#else
#include "windows.h"
#undef min
#undef max
#endif

#define IN_MVARSFIELDS argv[0]
#define IN_BVARSFIELDS argv[1]
#define IN_WVARSFIELDS argv[2]
#define IN_PNINT argv[3]
#define IN_RNINT argv[4]
#define IN_SNINT argv[5]
#define IN_CONV argv[6]
#define IN_NQ argv[7]
#define IN_AQUAD argv[8]
#define IN_MAXIT argv[9]
#define IN_STD argv[10]
#define IN_NCOV argv[11]
#define IN_RIDGEIN argv[12]
#define IN_MLS argv[13]

#define OUT_NOBS argv[14]

#define OUT_N_G argv[15]
#define OUT_G_MIN argv[16]
#define OUT_G_AVG argv[17]
#define OUT_G_MAX argv[18]

#define OUT_DEV_1 argv[19]
#define OUT_B_1 argv[20]
#define OUT_V_1 argv[21]
#define IN_RE_1 argv[22]
#define IN_RES_1 argv[23]
#define OUT_NITER_1 argv[24]

#define OUT_DEV_2 argv[25]
#define OUT_B_2 argv[26]
#define OUT_V_2 argv[27]
#define IN_RE_2 argv[28]
#define IN_RES_2 argv[29]
#define OUT_NITER_2 argv[30]

#define OUT_DEV_3 argv[31]
#define OUT_B_3 argv[32]
#define OUT_V_3 argv[33]
#define IN_RE_3 argv[34]
#define IN_RES_3 argv[35]
#define OUT_NITER_3 argv[36]

/* Original version of the DLL */
typedef void(* MIXREGLSR)(
	/* Inputs */
	int *IDS, /* ID variable (N x 1) */
	double *Y, /* Response  variable (N x 1) */
	double *X0, /* Mean covariates (N x P) */
	double *U0, /* Between-subject covariates (N x R) */
	double *W0, /* Within-subject covariates (N x S) */
	int* N, /* Number of rows in data */
	int* NC, /* Number of unique IDs */
	int* P, /* ncol(X0) */
	int* R, /* ncol(U0) */
	int* S, /* ncol(W0) */
	int* PNINT, /* Include intercept parameter in means covariates */
	int* RNINT, /* Include intercept parameter in between-subject covariates */
	int* SNINT, /* Include intercept parameter for within-subject covariates */
	double* CONV, /* Convergence tolerance */
	int *NQ, /* Number of quadrature points (2-20) */
	int *AQUAD, /* Adaptive quadrature (0 - No, 1 - Yes)*/
	int *MAXIT, /* Maximum number of iterations */
	int *NCENT, /* Standardise covariates (0 - Yes, 1 - No) */
	int *NCOV, /* Assocation type: 0 - None, 1 - Linear, 2 - Quadratic */

	/* Outputs */
	double* dev1, /* Model 1 - deviance  */
	double *beta1, /* Model 1 (mean) estimates (Padj x 1) */
	double *alpha1, /* Model 1 (between-subjects) estimates (Radj x 1_) */
	double *tau1, /* Model 1 (within-subjects) estimates (1 x 1) */
	double *se1, /* Model 1 standard errors (Padj + Radj + 1 x 1) */
	double *thet1, /* Model 1 random effects (NC x 1) */
	double *pvar1, /* Model 1 posterior variances (NC x 1) */
	double *zerr1, /* Model 1 standardised residuals (N x 1) */

	double* dev2, /* Model 2 - deviance */
	double *beta2, /* Model 2 (mean) estimates (Padj x 1) */
	double *alpha2, /* Model 2 (between-subjects) estimates (Radj x 1) */
	double *tau2, /* Model 2 (within-subjects) estimates (Sadj x 1) */
	double *se2, /* Model 2 standard errors (Padj + Radj + Sadj x 1) */
	double *thet2, /* Model 2 random effects (NC x 1) */
	double *pvar2, /* Model 2 posterior variances (NC x 1) */
	double *zerr2, /* Model 2 standardised residuals (N x 1) */

	double* dev3, /* Model 3 - deviance */
	double *beta3, /* Model 3 (mean) estimates (Padj x 1) */
	double *alpha3, /* Model 3 (between-subjects) estimates (Radj x 1) */
	double *tau3, /* Model 3 (within-subjects) estimates (Sadj x 1) */
	double *se3, /* Model 3 standard errors (Padj x Radj x Sadj + NCOV + 1 x 1) */
	double *spar3, /* Model 3 scale effect parameters (NCOV + 1 x 1) */
	double *thet3, /* Model 3 random effects (NC x 2) */
	double *pvar3, /* Model 3 posterior variances (NC x 3) */
	double *zerr3 /* Model 3 standardised residuals (N x 1) */
	);

/* Updated version of the DLL with ridgein, co-variance matrix output and number of iterations */
typedef void(* MIXREGLS)(
	/* Inputs */
	int *IDS, /* ID variable (N x 1) */
	double *Y, /* Response  variable (N x 1) */
	double *X0, /* Mean covariates (N x P) */
	double *U0, /* Between-subject covariates (N x R) */
	double *W0, /* Within-subject covariates (N x S) */
	int* N, /* Number of rows in data */
	int* NC, /* Number of unique IDs */
	int* P, /* ncol(X0) */
	int* R, /* ncol(U0) */
	int* S, /* ncol(W0) */
	int* PNINT, /* Include intercept parameter in means covariates */
	int* RNINT, /* Include intercept parameter in between-subject covariates */
	int* SNINT, /* Include intercept parameter for within-subject covariates */
	double* CONV, /* Convergence tolerance */
	int *NQ, /* Number of quadrature points (2-20) */
	int *AQUAD, /* Adaptive quadrature (0 - No, 1 - Yes)*/
	int *MAXIT, /* Maximum number of iterations */
	int *NCENT, /* Standardise covariates (0 - Yes, 1 - No) */
	int *NCOV, /* Assocation type: 0 - None, 1 - Linear, 2 - Quadratic */
	double *RIDGEIN, /* Initial value for ridge */

	/* Outputs */
	double* dev1, /* Model 1 - deviance  */
	double *beta1, /* Model 1 (mean) estimates (Padj x 1) */
	double *alpha1, /* Model 1 (between-subjects) estimates (Radj x 1_) */
	double *tau1, /* Model 1 (within-subjects) estimates (1 x 1) */
	double *se1, /* Model 1 standard errors (Padj + Radj + 1 x 1) */
	double *thet1, /* Model 1 random effects (NC x 1) */
	double *pvar1, /* Model 1 posterior variances (NC x 1) */
	double *zerr1, /* Model 1 standardised residuals (N x 1) */
	double *varcov1, /* Model 1 covariance matrix */
	int *niter1, /* Num iterations for Model 1 */

	double* dev2, /* Model 2 - deviance */
	double *beta2, /* Model 2 (mean) estimates (Padj x 1) */
	double *alpha2, /* Model 2 (between-subjects) estimates (Radj x 1) */
	double *tau2, /* Model 2 (within-subjects) estimates (Sadj x 1) */
	double *se2, /* Model 2 standard errors (Padj + Radj + Sadj x 1) */
	double *thet2, /* Model 2 random effects (NC x 1) */
	double *pvar2, /* Model 2 posterior variances (NC x 1) */
	double *zerr2, /* Model 2 standardised residuals (N x 1) */
	double *varcov2, /* Model 2 covariance matrix */
	int *niter2, /* Num iterations for Model 2 */

	double* dev3, /* Model 3 - deviance */
	double *beta3, /* Model 3 (mean) estimates (Padj x 1) */
	double *alpha3, /* Model 3 (between-subjects) estimates (Radj x 1) */
	double *tau3, /* Model 3 (within-subjects) estimates (Sadj x 1) */
	double *se3, /* Model 3 standard errors (Padj + Radj + Sadj + NCOV + 1 x 1) */
	double *spar3, /* Model 3 scale effect parameters (NCOV + 1 x 1) */
	double *thet3, /* Model 3 random effects (NC x 2) */
	double *pvar3, /* Model 3 posterior variances (NC x 3) */
	double *zerr3, /* Model 3 standardised residuals (N x 1) */
	double *varcov3, /* Model 3 covariance matrix */
	int *niter3  /* Num iterations for Model 3 */
);

typedef void(*INIT_DATA)(
	int* IDS, /* ID variable (N x 1) */
	double* Y, /* Response  variable (N x 1) */
	double* X0, /* Mean covariates (N x P) */
	double* U0, /* Between-subject covariates (N x R) */
	double* W0, /* Within-subject covariates (N x S) */
	int* nobs, /* number of observations */
	int* p, /* number of covariates */
	int* r, /* number of random effect variance terms */
	int* s, /* number of error variance terms */
	int* pnint, /* 1 if no intercept for mean model (0 otherwise) */
	int* rnint, /* 1 if no intercept for BS variance model  */
	int* snint, /* 1 if no intercept for WS variance model */
	int* ncent, /* standardise covariates (0 - Yes, 1 - No) */
	int* ncov, /* assocation type: 0 - None, 1 - Linear, 2 - Quadratic */
	int* mls /* multiple location effects (0 - No, 1 - Yes), also sets ncov to 1 */
);

typedef void(*INIT_PARAMS)(
	double *beta, /* fixed parameters (length P) */
	double *mychol, /* cholesky of random parameters (length RR) */
	double* tau, /* level-1 variance */
	double *thetas, /* level-2 residuals (NC2 x R) */
	double *thetasv /* level-2 residual variances (NC2 x RR) */
);

typedef void(*MIXREGMLSEST)(
	int* maxit, /* maximum number of iterations */
	double* ridgein, /* initial value of the ridge */
	double* conv,  /* Convergence tolerance */
	int* aquad, /* adaptive quadrature (0 - No 1 - Yes) */
	int* nq, /* number of quad pts (2 to 20) */
	double* dev1, /* Model 1 - deviance  */
	double *beta1, /* Model 1 (mean) estimates (Padj x 1) */
	double *alpha1, /* Model 1 (between-subjects) estimates (Radj x 1) */
	double *tau1, /* Model 1 (within-subjects) estimates (1 x 1) */
	double *thet1, /* Model 1 random effects (NC x 1) */
	double *pvar1, /* Model 1 posterior variances (NC x 1) */
	double *varcov1, /* Model 1 covariance matrix */
	int *niter1, /* Num iterations for Model 1 */

	double* dev2, /* Model 2 - deviance */
	double *beta2, /* Model 2 (mean) estimates (Padj x 1) */
	double *alpha2, /* Model 2 (between-subjects) estimates (Radj x 1) */
	double *tau2, /* Model 2 (within-subjects) estimates (Sadj x 1) */
	double *thet2, /* Model 2 random effects (NC x 1) */
	double *pvar2, /* Model 2 posterior variances (NC x 1) */
	double *varcov2, /* Model 2 covariance matrix */
	int *niter2, /* Num iterations for Model 2 */

	double* dev3, /* Model 3 - deviance */
	double *beta3, /* Model 3 (mean) estimates (Padj x 1) */
	double *alpha3, /* Model 3 (between-subjects) estimates (Radj x 1) */
	double *tau3, /* Model 3 (within-subjects) estimates (Sadj x 1) */
	double *spar3, /* Model 3 scale effect parameters (NCOV + 1 x 1) */
	double *thet3, /* Model 3 random effects (NC x 2) */
	double *pvar3, /* Model 3 posterior variances (NC x 3) */
	double *varcov3, /* Model 3 covariance matrix */
	int *niter3  /* Num iterations for Model 3 */
);

typedef void(*MIXREGLSEST)(
	int* maxit, /* maximum number of iterations */
	double* ridgein, /* initial value of the ridge */
	double* conv,  /* Convergence tolerance */
	int* aquad, /* adaptive quadrature (0 - No 1 - Yes) */
	int* nq, /* number of quad pts (2 to 20) */
	double* dev1, /* Model 1 - deviance  */
	double *beta1, /* Model 1 (mean) estimates (Padj x 1) */
	double *alpha1, /* Model 1 (between-subjects) estimates (Radj x 1) */
	double *tau1, /* Model 1 (within-subjects) estimates (1 x 1) */
	double *thet1, /* Model 1 random effects (NC x 1) */
	double *pvar1, /* Model 1 posterior variances (NC x 1) */
	double *varcov1, /* Model 1 covariance matrix */
	int *niter1, /* Num iterations for Model 1 */

	double* dev2, /* Model 2 - deviance */
	double *beta2, /* Model 2 (mean) estimates (Padj x 1) */
	double *alpha2, /* Model 2 (between-subjects) estimates (Radj x 1) */
	double *tau2, /* Model 2 (within-subjects) estimates (Sadj x 1) */
	double *thet2, /* Model 2 random effects (NC x 1) */
	double *pvar2, /* Model 2 posterior variances (NC x 1) */
	double *varcov2, /* Model 2 covariance matrix */
	int *niter2, /* Num iterations for Model 2 */

	double* dev3, /* Model 3 - deviance */
	double *beta3, /* Model 3 (mean) estimates (Padj x 1) */
	double *alpha3, /* Model 3 (between-subjects) estimates (Radj x 1) */
	double *tau3, /* Model 3 (within-subjects) estimates (Sadj x 1) */
	double *spar3, /* Model 3 scale effect parameters (NCOV + 1 x 1) */
	double *thet3, /* Model 3 random effects (NC x 2) */
	double *pvar3, /* Model 3 posterior variances (NC x 3) */
	double *varcov3, /* Model 3 covariance matrix */
	int *niter3  /* Num iterations for Model 3 */
);

// MixReg DLL for starting values
typedef void(*INIT)(int* nem, int* maxcol, int* nrow, double* data, int* r, int* p, double* conv, int* ifin, int* nauto, int* nomu, int* nocov, int* idind, int* xind, int* yind, int* iwind, int* maxni, int* timeind, double* time, int* ns, int* s, int* istart, double* imu, double* ialpha, double* ivarco, double* error, double* iauto);
typedef int(*MAINLOOP)(double* dev, double* zerr, double* zerr_cov);
typedef void(*CLEANUP)(double* beta, double* alpha, double* varcovr, double* varcovf);

STDLL stata_call(int argc, char *argv[])
{
	if (argc != 37) {
		SF_error("Incorrect number of arguments passed to function\n");
		return 198;
	}

	ST_retcode rc = 0;
	double inval = 0;

	/* Input variables */
	int P = SF_col(IN_MVARSFIELDS);
	if (P < 0) {
		SF_error("P must be positive\n");
		return 198;
	}
	int R = SF_col(IN_BVARSFIELDS);
	if (R < 0) {
		SF_error("R must be positive\n");
		return 198;
	}
	int S = SF_col(IN_WVARSFIELDS);
	if (S < 0) {
		SF_error("S must be positive\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_PNINT, &inval))) return(rc);
	int PNINT = (int)inval;
	if (PNINT < 0 || PNINT > 1) {
		SF_error("PNINT must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_RNINT, &inval))) return(rc);
	int RNINT = (int)inval;
	if (RNINT < 0 || RNINT > 1) {
		SF_error("RNINT must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_SNINT, &inval))) return(rc);
	int SNINT = (int)inval;
	if (SNINT < 0 || SNINT > 1) {
		SF_error("SNINT must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_CONV, &inval))) return(rc);
	double CONV = inval;
	if (CONV <= 0) {
		SF_error("CONV must be positive\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_NQ, &inval))) return(rc);
	int NQ = (int)inval;
	if (NQ <= 0) {
		SF_error("NQ must be positive\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_AQUAD, &inval))) return(rc);
	int AQUAD = (int)inval;
	if (AQUAD < 0 || AQUAD > 1) {
		SF_error("AQUAD must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_MAXIT, &inval))) return(rc);
	int MAXIT = (int)inval;
	if (MAXIT <= 0) {
		SF_error("MAXIT must be positive\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_STD, &inval))) return(rc);
	int STD = (int)inval;
	if (STD < 0 || STD > 1) {
		SF_error("STD must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_NCOV, &inval))) return(rc);
	int NCOV = (int)inval;
	if (NCOV < 0 || NCOV > 2) {
		SF_error("NCOV must be zero, one or two\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_RIDGEIN, &inval))) return(rc);
	double RIDGEIN = inval;
	if ((rc = SF_scal_use(IN_MLS, &inval))) return(rc);
	int MLS = (int)inval;
	if (MLS < 0 || MLS > 1) {
		SF_error("MLS must be zero or one\n");
		return 198;
	}

	if (MLS == 1 && NCOV != 1) {
		NCOV = 1; // Only NCOV == 1 is supported for MLS estimation
		SF_display("NCOV setting not supported with MLS, setting to 1");
	}

	/* Indicators whether to export random effects */
	if ((rc = SF_scal_use(IN_RE_1, &inval))) return(rc);
	int store_rand_eff_1 = (int)inval;
	if (store_rand_eff_1 < 0 || store_rand_eff_1 > 1) {
		SF_error("store_rand_eff_1 must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_RE_2, &inval))) return(rc);
	int store_rand_eff_2 = (int)inval;
	if (store_rand_eff_2 < 0 || store_rand_eff_2 > 1) {
		SF_error("store_rand_eff_2 must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_RE_3, &inval))) return(rc);
	int store_rand_eff_3 = (int)inval;
	if (store_rand_eff_3 < 0 || store_rand_eff_3 > 1) {
		SF_error("store_rand_eff_3 must be zero or one\n");
		return 198;
	}

	/* Indicators whether to export residuals */
	if ((rc = SF_scal_use(IN_RES_1, &inval))) return(rc);
	int store_resi_1 = (int)inval;
	if (store_resi_1 < 0 || store_resi_1 > 1) {
		SF_error("store_resi_1 must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_RES_2, &inval))) return(rc);
	int store_resi_2 = (int)inval;
	if (store_resi_2 < 0 || store_resi_2 > 1) {
		SF_error("store_resi_2 must be zero or one\n");
		return 198;
	}
	if ((rc = SF_scal_use(IN_RES_3, &inval))) return(rc);
	int store_resi_3 = (int)inval;
	if (store_resi_3 < 0 || store_resi_3 > 1) {
		SF_error("store_resi_3 must be zero or one\n");
		return 198;
	}

	int Padj = P + 1 - PNINT;
	if (Padj <= 0) {
		SF_error("No variables specified for mean parameters\n");
		return 198;
	}
	int Radj = R + 1 - RNINT;
	if (Radj <= 0) {
		SF_error("No variables specified for between parameters\n");
		return 198;
	}
	int Sadj = S + 1 - SNINT;
	if (Sadj <= 0) {
		SF_error("No variables specified for within parameters\n");
		return 198;
	}

	int nvars = 2; /* ID + response */
	std::vector<int> uniqvars;
	//for (int i = 1; i <= SF_nvars(); ++i) {
		for (int k = 1; k <= P; ++k) {
			double colnum = 0;
			if ((rc = SF_mat_el(IN_MVARSFIELDS, 1, k, &colnum))) return(rc);
			uniqvars.push_back(static_cast<int>(colnum));
		}
		for (int k = 1; k <= R; ++k) {
			double colnum = 0;
			if ((rc = SF_mat_el(IN_BVARSFIELDS, 1, k, &colnum))) return(rc);
			uniqvars.push_back(static_cast<int>(colnum));
		}
		for (int k = 1; k <= S; ++k) {
			double colnum = 0;
			if ((rc = SF_mat_el(IN_WVARSFIELDS, 1, k, &colnum))) return(rc);
			uniqvars.push_back(static_cast<int>(colnum));
		}
	//}
	std::sort(uniqvars.begin(), uniqvars.end());
	auto it = std::unique(uniqvars.begin(), uniqvars.end());
	uniqvars.resize(it - uniqvars.begin());
	nvars += static_cast<int>(uniqvars.size());

	for (int i = 1; i <= SF_nvars(); ++i) {
		if (SF_var_is_string(i)) {
			SF_error("Input variables must all be numeric");
			return 198;
		}
	}

	// Calculate number of records in use
	int N = 0;
	for (int j = SF_in1(); j <= SF_in2(); ++j) {
		if SF_ifobs(j) {
			bool use = true;
			for (int i = 1; i <= nvars; ++i) {
				double val = 0;
				if ((rc = SF_vdata(i, j, &val))) return(rc);
				if (SF_is_missing(val)) {
					use = false;
				}
			}
			if (use == true) {
				++N;
			}
		}
	}
	if ((rc = SF_scal_save(OUT_NOBS, N))) return(rc);

	if (N <= 0) {
		SF_error("No observations\n");
		return 198;
	}

	int libver = 3;

	// Load library
#ifdef _MSC_VER
	wchar_t strExePath[MAX_PATH];
	GetModuleFileName(GetModuleHandle(L"mixregls.plugin"), strExePath, MAX_PATH);
	wchar_t drive[_MAX_DRIVE];
	wchar_t dir[_MAX_DIR];
	wchar_t fname[_MAX_FNAME];
	wchar_t ext[_MAX_EXT];

	_wsplitpath_s(strExePath, drive, dir, fname, ext);

	static wchar_t libpath[MAX_PATH + 1];
	swprintf(libpath, MAX_PATH, L"%s%s", drive, dir);

	SetDllDirectory(libpath);
	HINSTANCE HLib = LoadLibrary(L"mixregls");
	SetDllDirectory(nullptr);
#else
	Dl_info dl_info;
	dladdr((void*)stata_call, &dl_info);
	std::string path = dl_info.dli_fname;
	auto spos = path.rfind("/");
	if (spos != std::string::npos) {
		path = path.substr(0, spos);
	}
	else {
		SF_error("error finding library path\n");
	}

#ifndef __APPLE__
	std::string lib = path + "/mixregls.so.1";
#else
	std::string lib = path + "/mixregls.dylib";
#endif

	void *HLib = dlopen(lib.c_str(), RTLD_LAZY);
#endif

	MIXREGLSR mixreglsr = nullptr;
	MIXREGLS mixregls = nullptr;

	if (HLib == nullptr) {
		SF_error("Unable to load mixregls library\n");
#ifdef _MSC_VER
#else
		char *err = dlerror();
		if (err != nullptr) {
			SF_error(err);
		}
#endif
		return 198;
	}

#ifdef _MSC_VER
	const INIT_DATA init_data = (INIT_DATA)GetProcAddress(HLib, "init_data");
	const INIT_PARAMS init_params = (INIT_PARAMS)GetProcAddress(HLib, "init_params");
	const MIXREGMLSEST mixregmlsest = (MIXREGMLSEST)GetProcAddress(HLib, "mixregmlsest");
	const MIXREGLSEST mixreglsest = (MIXREGLSEST)GetProcAddress(HLib, "mixreglsest");
#else
	const INIT_DATA init_data = (INIT_DATA)dlsym(HLib, "init_data");
	const INIT_PARAMS init_params = (INIT_PARAMS)dlsym(HLib, "init_params");
	const MIXREGMLSEST mixregmlsest = (MIXREGMLSEST)dlsym(HLib, "mixregmlsest");
	const MIXREGLSEST mixreglsest = (MIXREGLSEST)dlsym(HLib, "mixreglsest");
#endif

	if (!init_data || !init_params || !mixregmlsest || !mixreglsest) {
		libver = 2;
#ifdef _MSC_VER
		mixregls = (MIXREGLS)GetProcAddress(HLib, "mixregls");
#else
		mixregls = (MIXREGLS)dlsym(HLib, "mixregls");
#endif
		if (!mixregls) {
			libver = 1;
#ifdef _MSC_VER
			mixreglsr = (MIXREGLSR)GetProcAddress(HLib, "mixreglsr");
#else
			mixreglsr = (MIXREGLSR)dlsym(HLib, "mixreglsr");
#endif
			if (!mixreglsr) {
				SF_error("Unable to load mixreglsr function");
#ifdef _MSC_VER
				FreeLibrary(HLib);
#else
				char *err = dlerror();
				if (err != nullptr) {
					SF_error(err);
				}
				dlclose(HLib);
#endif
				return 198;
			}
		}
	}

	// Copy data
	int nfield = 1 + 1 + P + R + 1;
	std::vector<double> indata(nfield * N);
	int idind = 1;
	int yind = 2;
	std::vector<int> pind(Padj);
	std::vector<int> rind(Radj);

	std::vector<int> IDS(N);
	std::vector<double> Y(N);
	std::vector<double> X0(N*P);
	if (X0.size() == 0) { // We pass the first element to the underlying function, so make sure it exists
		X0.resize(N);
	}
	std::vector<double> U0(N*R);
	if (U0.size() == 0) { // We pass the first element to the underlying function, so make sure it exists
		U0.resize(N);
	}
	std::vector<double> W0(N*S);
	if (W0.size() == 0) { // We pass the first element to the underlying function, so make sure it exists
		W0.resize(N);
	}
	int row = 0;
	for (int j = SF_in1(); j <= SF_in2(); ++j) {
		if SF_ifobs(j) {
			bool use = true;
			for (int i = 1; i <= nvars; ++i) {
				double val = 0;
				if ((rc = SF_vdata(i, j, &val))) return(rc);
				if (SF_is_missing(val)) {
					use = false;
				}
			}
			if (use == true) {
				double val = 0;
				if ((rc = SF_vdata(idind, j, &val))) return(rc);
				IDS[row] = (int)val;
				indata[((idind - 1) * N) + row] = val;
				if ((rc = SF_vdata(yind, j, &val))) return(rc);
				Y[row] = val;
				indata[((yind - 1) * N) + row] = val;
				for (int k = 1; k <= P; ++k) {
					double colnum = 0;
					if ((rc = SF_mat_el(IN_MVARSFIELDS, 1, k, &colnum))) return(rc);
					if ((rc = SF_vdata(static_cast<int>(colnum), j, &val))) return(rc);
					X0[((k - 1)*N) + row] = val;

					indata[((static_cast<int>(colnum) - 1)*N) + row] = val;
					pind[k - 1 * PNINT] = static_cast<int>(colnum);
				}
				if (PNINT == 0) {
					pind[0] = nfield;
				}
				for (int k = 1; k <= R; ++k) {
					double colnum = 0;
					if ((rc = SF_mat_el(IN_BVARSFIELDS, 1, k, &colnum))) return(rc);
					if ((rc = SF_vdata(static_cast<int>(colnum), j, &val))) return(rc);
					U0[((k - 1)*N) + row] = val;

					indata[((static_cast<int>(colnum) - 1) * N) + row] = val;
					rind[k - 1 * RNINT] = static_cast<int>(colnum);
				}
				if (RNINT == 0) {
					rind[0] = nfield;
				}
				for (int k = 1; k <= S; ++k) {
					double colnum = 0;
					if ((rc = SF_mat_el(IN_WVARSFIELDS, 1, k, &colnum))) return(rc);
					if ((rc = SF_vdata(static_cast<int>(colnum), j, &val))) return(rc);
					W0[((k - 1)*N) + row] = val;
				}
				indata[((nfield - 1) * N) + row] = 1.0;
				++row;
			}
		}
	}

	// Calculate group information;
	int g_min = INT_MAX; // Maximum group size
	int g_max = INT_MIN; // Minimum group size
	int N_g = 1; // Number of groups
	int count = 0; // Number of records in current group
	for (int j = 1; j < N; ++j) {
		if (IDS[j] != IDS[j - 1]) { // New group found
			++N_g;
			g_min = std::min(count, g_min);
			g_max = std::max(count, g_max);
			count = 0;
		}
		++count;
	}

	if ((rc = SF_scal_save(OUT_N_G, N_g))) return(rc);
	g_min = std::min(count, g_min);
	if ((rc = SF_scal_save(OUT_G_MIN, g_min))) return(rc);
	g_max = std::max(count, g_max);
	if ((rc = SF_scal_save(OUT_G_MAX, g_max))) return(rc);
	double g_mean = (double)N / (double)N_g; // Average number of records per group
	if ((rc = SF_scal_save(OUT_G_AVG, g_mean))) return(rc);

	int rparams = (MLS == 1) ? Radj : 1;
	int rvparams = rparams * (rparams + 1) / 2;

	int rparams3 = rparams + 1;
	int rvparams3 = rparams3 * (rparams3 + 1) / 2;
	
	int ns = NCOV + 1;
	if (MLS == 1) {
		ns = (NCOV * Radj) + 1;
		Radj = Radj * (Radj + 1) / 2;
	}

	/* Output variables */
	double dev1 = 0;
	std::vector<double> beta1(Padj);
	std::vector<double> alpha1(Radj);
	std::vector<double> tau1(1);
	if (libver == 1) {
		tau1.resize(Sadj); // Original DLL overwrote extra elements here
	}
	std::vector<double> se1(Padj + Radj + 1);
	std::vector<double> thet1(N_g * rparams);
	std::vector<double> pvar1(N_g * rvparams);
	std::vector<double> zerr1(N);
	std::vector<double> varcov1((Padj + Radj + 1)*(Padj + Radj + 1));
	int niter1 = 0;

	double dev2 = 0;
	std::vector<double> beta2(Padj);
	std::vector<double> alpha2(Radj);
	std::vector<double> tau2(Sadj);
	std::vector<double> se2(Padj + Radj + Sadj);
	std::vector<double> thet2(N_g * rparams);
	std::vector<double> pvar2(N_g * rvparams);
	std::vector<double> zerr2(N);
	std::vector<double> varcov2((Padj + Radj + Sadj)*(Padj + Radj + Sadj));
	int niter2 = 0;

	double dev3 = 0;
	std::vector<double> beta3(Padj);
	std::vector<double> alpha3(Radj);
	std::vector<double> tau3(Sadj);
	std::vector<double> se3(Padj + Radj + Sadj + ns);
	std::vector<double> spar3(ns);
	std::vector<double> thet3(N_g * rparams3);
	std::vector<double> pvar3(N_g * rvparams3);
	std::vector<double> zerr3(N);
	std::vector<double> varcov3((Padj + Radj + Sadj + ns)*(Padj + Radj + Sadj + ns));
	int niter3 = 0;

	bool done = false;
	int displayed_iter1 = 0;
	int displayed_iter2 = 0;
	int displayed_iter3 = 0;

	if (libver == 3) {

#ifdef _MSC_VER
		SetDllDirectory(libpath);
		HINSTANCE HLib2 = LoadLibrary(L"mixreg");
		SetDllDirectory(nullptr);
#else
#ifndef __APPLE__
		std::string lib2 = path + "/mixreg.so.1";
#else
		std::string lib2 = path + "/mixreg.dylib";
#endif
		void* HLib2 = dlopen(lib2.c_str(), RTLD_LAZY);
#endif

		if (HLib2 == nullptr) {
			SF_error("Unable to load mixreg libraries\n");
#ifdef _MSC_VER
#else
			char* err = dlerror();
			if (err != nullptr) {
				SF_error(err);
			}
#endif
			return 198;
		}

#ifdef _MSC_VER
		const INIT init = (INIT)GetProcAddress(HLib2, "__rrm_module_MOD_init");
		const MAINLOOP mainloop = (MAINLOOP)GetProcAddress(HLib2, "__rrm_module_MOD_mainloop");
		const CLEANUP cleanup = (CLEANUP)GetProcAddress(HLib2, "__rrm_module_MOD_cleanup");
#else
		const INIT init = (INIT)dlsym(HLib, "__rrm_module_MOD_init");
		const MAINLOOP mainloop = (MAINLOOP)dlsym(HLib, "__rrm_module_MOD_mainloop");
		const CLEANUP cleanup = (CLEANUP)dlsym(HLib, "__rrm_module_MOD_cleanup");
#endif

		if (!init || !mainloop || !cleanup) {
			SF_error("Unable to load mixreg functions");
#ifdef _MSC_VER
			FreeLibrary(HLib2);
#else
			char* err = dlerror();
			if (err != nullptr) {
				SF_error(err);
			}
			dlclose(HLib2);
#endif
			return 198;
		}


		// Calculate starting values from MixReg
		int nloc = rparams; //(MLS == 1) ? Radj : 1;
		int RR = (nloc + 1) * nloc / 2;

		int NEM = 20;
		int IFIN = 0;
		int AUTO = 0;
		int NOMU = 1;
		int NOCOV = 0;
		int istart = 0;
		std::vector<double> imu(nloc);
		std::vector<double> ialpha(Padj);
		std::vector<double> ivarco(RR);
		double ierror = 0;
		int TS = 1;
		int NS = 0;
		int maxni = 0;
		int timeind = 0;
		std::vector<double> time(1);
		std::vector<double> iauto(TS);

		// results used as starting values
		double dev_start = 0;
		std::vector<double> theta_start(N_g * rparams);
		std::vector<double> thetav_start(N_g * rvparams);
		std::vector<double> alpha_start(Padj);
		std::vector<double> beta_start(RR + 1);

		// covariance matrices not used for starting values
		std::vector<double> varcovf(Padj * Padj);
		std::vector<double> varcovr((RR + 1) * (RR + 1));
		init(&NEM, &nfield, &N, &indata[0], &nloc, &Padj, &CONV, &IFIN, &AUTO, &NOMU, &NOCOV, &idind, &yind, &rind[0], &pind[0], &maxni, &timeind, &time[0], &NS, &S, &istart, &imu[0], &ialpha[0], &ivarco[0], &ierror, &iauto[0]);
		SF_display("0");
		int niter = 0;
		while (mainloop(&dev_start, &theta_start[0], &thetav_start[0]) != 0) {
			SF_poll();
			//if (niter >= MAXIT) {
			//	IFIN += 2;
			//}
			//if (SW_stopflag) {
			//	SF_display("Aborted\n");
			//	SF_flush();
			//	break;
			//}
			SF_display(".");
			++niter;
		}
		cleanup(&beta_start[0], &alpha_start[0], &varcovr[0], &varcovf[0]);

#ifdef _MSC_VER
		FreeLibrary(HLib2);
#else
		dlclose(HLib2);
#endif

		double tau_start = beta_start[RR];

		if (MLS == 1) {
			niter1 = niter;
			dev1 = dev_start;
			for (int i = 0; i < Padj; i++) {
				beta1[i] = alpha_start[i];
			}
			for (int i = 0; i < Radj; i++) {
				alpha1[i] = beta_start[i];
			}
			tau1[0] = std::log(tau_start);
			for (int i = 0; i < Padj; i++) {
				se1[i] = sqrt(varcovf[i * Padj + i]);
				for (int j = 0; j < Padj; j++) {
					varcov1[i * (Padj + Radj + 1) + j] = varcovf[i * Padj + j];
				}
			}
			varcovr[varcovr.size() - 1] = std::pow(std::sqrt(varcovr[varcovr.size() - 1]) / beta_start[RR], 2);
			for (int i = 0; i < Radj + 1; i++) {
				se1[i + Padj] = sqrt(varcovr[i * (Radj + 1) + i]);
				for (int j = 0; j < Radj + 1; j++) {
					varcov1[(i + Padj) * (Padj + Radj + 1) + (j + Padj)] = varcovr[i * (Radj + 1) + j];
				}
			}
			for (int i = 0; i < N_g * rparams; i++) {
				thet1[i] = theta_start[i];
			}
			for (int i = 0; i < N_g * rvparams; i++) {
				pvar1[i] = thetav_start[i];
			}
		}

		bool done = false;

		init_data(&IDS[0], &Y[0], &X0[0], &U0[0], &W0[0], &N, &P, &R, &S, &PNINT, &RNINT, &SNINT, &STD, &NCOV, &MLS);
		init_params(&alpha_start[0], &beta_start[0], &tau_start, &theta_start[0], &thetav_start[0]);
	}

#if SYSTEM==OPUNIX && (__GLIBC__ < 2 || (__GLIBC__ == 2 && __GLIBC_MINOR__ <= 25))
	// Avoid crash when dynamically loading pthread (see https://sourceware.org/bugzilla/show_bug.cgi?id=16628)
	SF_display("Runnning Model...");
	SF_flush();
	if (libver == 3) {
		if (MLS == 1) {
			mixregmlsest(&MAXIT, &MAXIT, &RIDGEIN, &CONV, &AQUAD, &NQ,
				&dev1, &beta1[0], &alpha1[0], &tau1[0], &thet1[0], &pvar1[0], &varcov1[0], &niter1,
				&dev2, &beta2[0], &alpha2[0], &tau2[0], &thet2[0], &pvar2[0], &varcov2[0], &niter2,
				&dev3, &beta3[0], &alpha3[0], &tau3[0], &spar3[0], &thet3[0], &pvar3[0], &varcov3[0], &niter3);
		}
		else {
			mixreglsest(&MAXIT, &MAXIT, &RIDGEIN, &CONV, &AQUAD, &NQ,
				&dev1, &beta1[0], &alpha1[0], &tau1[0], &thet1[0], &pvar1[0], &varcov1[0], &niter1,
				&dev2, &beta2[0], &alpha2[0], &tau2[0], &thet2[0], &pvar2[0], &varcov2[0], &niter2,
				&dev3, &beta3[0], &alpha3[0], &tau3[0], &spar3[0], &thet3[0], &pvar3[0], &varcov3[0], &niter3);
		}
	}
	if (libver == 2) {
		mixregls(
			&IDS[0], &Y[0], &X0[0], &U0[0], &W0[0], &N, &N_g, &P, &R, &S, &PNINT, &RNINT, &SNINT, &CONV, &NQ, &AQUAD, &MAXIT, &STD, &NCOV, &RIDGEIN,
			&dev1, &beta1[0], &alpha1[0], &tau1[0], &se1[0], &thet1[0], &pvar1[0], &zerr1[0], &varcov1[0], &niter1,
			&dev2, &beta2[0], &alpha2[0], &tau2[0], &se2[0], &thet2[0], &pvar2[0], &zerr2[0], &varcov2[0], &niter2,
			&dev3, &beta3[0], &alpha3[0], &tau3[0], &se3[0], &spar3[0], &thet3[0], &pvar3[0], &zerr3[0], &varcov3[0], &niter3);
	}
	if (libver == 1) {
		mixreglsr(
			&IDS[0], &Y[0], &X0[0], &U0[0], &W0[0], &N, &N_g, &P, &R, &S, &PNINT, &RNINT, &SNINT, &CONV, &NQ, &AQUAD, &MAXIT, &STD, &NCOV,
			&dev1, &beta1[0], &alpha1[0], &tau1[0], &se1[0], &thet1[0], &pvar1[0], &zerr1[0],
			&dev2, &beta2[0], &alpha2[0], &tau2[0], &se2[0], &thet2[0], &pvar2[0], &zerr2[0],
			&dev3, &beta3[0], &alpha3[0], &tau3[0], &se3[0], &spar3[0], &thet3[0], &pvar3[0], &zerr3[0]);
	}
#else
	if (libver == 1) {
		SF_display("Runnning Model...");
		SF_flush();
	}

	// The following allows Stata to remain responsive while the function runs
	std::future<void> thd;
	if (libver == 3) {
		if (MLS == 1) {
			thd = std::async(std::launch::async, mixregmlsest, &MAXIT, &RIDGEIN, &CONV, &AQUAD, &NQ,
				&dev1, &beta1[0], &alpha1[0], &tau1[0], &thet1[0], &pvar1[0], &varcov1[0], &niter1,
				&dev2, &beta2[0], &alpha2[0], &tau2[0], &thet2[0], &pvar2[0], &varcov2[0], &niter2,
				&dev3, &beta3[0], &alpha3[0], &tau3[0], &spar3[0], &thet3[0], &pvar3[0], &varcov3[0], &niter3);
		}
		else {
			thd = std::async(std::launch::async, mixreglsest, &MAXIT, &RIDGEIN, &CONV, &AQUAD, &NQ,
				&dev1, &beta1[0], &alpha1[0], &tau1[0], &thet1[0], &pvar1[0], &varcov1[0], &niter1,
				&dev2, &beta2[0], &alpha2[0], &tau2[0], &thet2[0], &pvar2[0], &varcov2[0], &niter2,
				&dev3, &beta3[0], &alpha3[0], &tau3[0], &spar3[0], &thet3[0], &pvar3[0], &varcov3[0], &niter3);
		}
	}
	if (libver == 2) {
		thd = std::async(std::launch::async, mixregls,
			&IDS[0], &Y[0], &X0[0], &U0[0], &W0[0], &N, &N_g, &P, &R, &S, &PNINT, &RNINT, &SNINT, &CONV, &NQ, &AQUAD, &MAXIT, &STD, &NCOV, &RIDGEIN,
			&dev1, &beta1[0], &alpha1[0], &tau1[0], &se1[0], &thet1[0], &pvar1[0], &zerr1[0], &varcov1[0], &niter1,
			&dev2, &beta2[0], &alpha2[0], &tau2[0], &se2[0], &thet2[0], &pvar2[0], &zerr2[0], &varcov2[0], &niter2,
			&dev3, &beta3[0], &alpha3[0], &tau3[0], &se3[0], &spar3[0], &thet3[0], &pvar3[0], &zerr3[0], &varcov3[0], &niter3);
	}
	if (libver == 1) {
		thd = std::async(std::launch::async, mixreglsr,
			&IDS[0], &Y[0], &X0[0], &U0[0], &W0[0], &N, &N_g, &P, &R, &S, &PNINT, &RNINT, &SNINT, &CONV, &NQ, &AQUAD, &MAXIT, &STD, &NCOV,
			&dev1, &beta1[0], &alpha1[0], &tau1[0], &se1[0], &thet1[0], &pvar1[0], &zerr1[0],
			&dev2, &beta2[0], &alpha2[0], &tau2[0], &se2[0], &thet2[0], &pvar2[0], &zerr2[0],
			&dev3, &beta3[0], &alpha3[0], &tau3[0], &se3[0], &spar3[0], &thet3[0], &pvar3[0], &zerr3[0]);
	}
	
	while (done == false) {
		done = thd.wait_for(std::chrono::milliseconds(10)) == std::future_status::ready;
		if (libver == 2 || libver == 3) {
			for (int i = displayed_iter1; i < niter1; ++i) {
				if (displayed_iter1 == 0) {
					SF_display("1");
				}
				SF_display(".");
				++displayed_iter1;
			}
			for (int i = displayed_iter2; i < niter2; ++i) {
				if (displayed_iter2 == 0) {
					SF_display("2");
				}
				SF_display(".");
				++displayed_iter2;
			}
			for (int i = displayed_iter3; i < niter3; ++i) {
				if (displayed_iter3 == 0) {
					SF_display("3");
				}
				SF_display(".");
				++displayed_iter3;
			}
			SF_flush();
		}

		SF_poll();
		if (SW_stopflag) {
			if (MAXIT != 0) {
				SF_display("Aborted\n");
				SF_flush();
			}
			MAXIT = 0;
		}
#ifdef _MSC_VER
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
#endif
	}
#endif

	if (MAXIT != 0) {
		SF_display("Finished\n");
	}
	SF_flush();

	// NOTE: some of the input parameters get changed in the call above, so don't trust them after here
	// Unload library
#ifdef _MSC_VER
	FreeLibrary(HLib);
#else
	dlclose(HLib);
#endif

	/* Return results to Stata */

	if ((rc = SF_scal_save(OUT_DEV_1, dev1))) return(rc);
	int pos = 1;
	for (int i = 0; i < Padj; ++i) {
		if ((rc = SF_mat_store(OUT_B_1, 1, pos, beta1[i]))) return(rc);
		++pos;
	}
	for (int i = 0; i < Radj; ++i) {
		if ((rc = SF_mat_store(OUT_B_1, 1, pos, alpha1[i]))) return(rc);
		++pos;
	}
	if ((rc = SF_mat_store(OUT_B_1, 1, pos, tau1[0]))) return(rc);

	if (libver == 1) {
		for (int i = 0; i < Padj + Radj + 1; ++i) {
			if ((rc = SF_mat_store(OUT_V_1, i + 1, i + 1, se1[i] * se1[i]))) return(rc);
		}
	}

	if (libver == 2 || libver == 3) {
		int k1 = 0;
		for (int i = 0; i < Padj + Radj + 1; ++i) {
			for (int j = 0; j < Padj + Radj + 1; ++j) {
				if ((rc = SF_mat_store(OUT_V_1, i + 1, j + 1, varcov1[k1]))) return(rc);
				++k1;
			}
		}
	}
	if ((rc = SF_scal_save(OUT_NITER_1, niter1))) return(rc);

	if ((rc = SF_scal_save(OUT_DEV_2, dev2))) return(rc);
	pos = 1;
	for (int i = 0; i < Padj; ++i) {
		if ((rc = SF_mat_store(OUT_B_2, 1, pos, beta2[i]))) return(rc);
		++pos;
	}
	for (int i = 0; i < Radj; ++i) {
		if ((rc = SF_mat_store(OUT_B_2, 1, pos, alpha2[i]))) return(rc);
		++pos;
	}
	for (int i = 0; i < Sadj; ++i) {
		if ((rc = SF_mat_store(OUT_B_2, 1, pos, tau2[i]))) return(rc);
		++pos;
	}

	if (libver == 1) {
		for (int i = 0; i < Padj + Radj + Sadj; ++i) {
			if ((rc = SF_mat_store(OUT_V_2, i + 1, i + 1, se2[i] * se2[i]))) return(rc);
		}
	}

	if (libver == 2 || libver == 3) {
		int k2 = 0;
		for (int i = 0; i < Padj + Radj + Sadj; ++i) {
			for (int j = 0; j < Padj + Radj + Sadj; ++j) {
				if ((rc = SF_mat_store(OUT_V_2, i + 1, j + 1, varcov2[k2]))) return(rc);
				++k2;
			}
		}
	}
	if ((rc = SF_scal_save(OUT_NITER_2, niter2))) return(rc);

	if ((rc = SF_scal_save(OUT_DEV_3, dev3))) return(rc);
	pos = 1;
	for (int i = 0; i < Padj; ++i) {
		if ((rc = SF_mat_store(OUT_B_3, 1, pos, beta3[i]))) return(rc);
		++pos;
	}
	for (int i = 0; i < Radj; ++i) {
		if ((rc = SF_mat_store(OUT_B_3, 1, pos, alpha3[i]))) return(rc);
		++pos;
	}
	for (int i = 0; i < Sadj; ++i) {
		if ((rc = SF_mat_store(OUT_B_3, 1, pos, tau3[i]))) return(rc);
		++pos;
	}
	for (int i = 0; i < ns; ++i) {
		if ((rc = SF_mat_store(OUT_B_3, 1, pos, spar3[i]))) return(rc);
		++pos;
	}	

	if (libver == 1) {
		for (int i = 0; i < Padj + Radj + Sadj + ns; ++i) {
			if ((rc = SF_mat_store(OUT_V_3, i + 1, i + 1, se3[i] * se3[i]))) return(rc);
		}
	}

	if (libver == 2 || libver == 3) {
		int k3 = 0;
		for (int i = 0; i < Padj + Radj + Sadj + ns; ++i) {
			for (int j = 0; j < Padj + Radj + Sadj + ns; ++j) {
				if ((rc = SF_mat_store(OUT_V_3, i + 1, j + 1, varcov3[k3]))) return(rc);
				++k3;
			}
		}
	}
	if ((rc = SF_scal_save(OUT_NITER_3, niter3))) return(rc);

	int varnum = nvars + 1;

	if (store_rand_eff_1 == 1) {
		int row = 0;
		int group = 0;
		for (int j = SF_in1(); j <= SF_in2(); ++j) {
			if SF_ifobs(j) {
				bool use = true;
				for (int i = 1; i <= nvars; ++i) {
					double val = 0;
					if ((rc = SF_vdata(i, j, &val))) return(rc);
					if (SF_is_missing(val)) {
						use = false;
					}
				}
				if (use == true) {
					if (row > 0 && IDS[row] != IDS[row - 1]) {
						++group;
					}
					int idx = 0;
					for (int k = 0; k < rparams; ++k) {
						if ((rc = SF_vstore(varnum + idx, j, thet1[(N_g * k) + group]))) return(rc);
						++idx;
					}
					for (int k = 0; k < rvparams; ++k) {
						if ((rc = SF_vstore(varnum + idx, j, pvar1[(N_g * k) + group]))) return(rc);
						++idx;
					}
					//if ((rc = SF_vstore(varnum, j, thet1[group]))) return(rc);
					//if ((rc = SF_vstore(varnum + 1, j, thet1[group + N_g]))) return(rc);
					//if ((rc = SF_vstore(varnum + 2, j, pvar1[group]))) return(rc);
					//if ((rc = SF_vstore(varnum + 3, j, pvar1[group + N_g]))) return(rc);
					//if ((rc = SF_vstore(varnum + 4, j, pvar1[group + (N_g * 2)]))) return(rc);
					++row;
				}
			}
		}
		//varnum += 5;
		varnum += rparams + rvparams;
	}

	if (store_rand_eff_2 == 1) {
		int row = 0;
		int group = 0;
		for (int j = SF_in1(); j <= SF_in2(); ++j) {
			if SF_ifobs(j) {
				bool use = true;
				for (int i = 1; i <= nvars; ++i) {
					double val = 0;
					if ((rc = SF_vdata(i, j, &val))) return(rc);
					if (SF_is_missing(val)) {
						use = false;
					}
				}
				if (use == true) {
					if (row > 0 && IDS[row] != IDS[row - 1]) {
						++group;
					}
					int idx = 0;
					for (int k = 0; k < rparams; ++k) {
						if ((rc = SF_vstore(varnum + idx, j, thet2[(N_g * k) + group]))) return(rc);
						++idx;
					}
					for (int k = 0; k < rvparams; ++k) {
						if ((rc = SF_vstore(varnum + idx, j, pvar2[(N_g * k) + group]))) return(rc);
						++idx;
					}
					//if ((rc = SF_vstore(varnum, j, thet2[group]))) return(rc);
					//if ((rc = SF_vstore(varnum + 1, j, thet2[group + N_g]))) return(rc);
					//if ((rc = SF_vstore(varnum + 2, j, pvar2[group]))) return(rc);
					//if ((rc = SF_vstore(varnum + 3, j, pvar2[group + N_g]))) return(rc);
					//if ((rc = SF_vstore(varnum + 4, j, pvar2[group + (N_g * 2)]))) return(rc);
					++row;
				}
			}
		}
		//varnum += 5;
		varnum += rparams + rvparams;
	}

	if (store_rand_eff_3 == 1) {
		int row = 0;
		int group = 0;
		for (int j = SF_in1(); j <= SF_in2(); ++j) {
			if SF_ifobs(j) {
				bool use = true;
				for (int i = 1; i <= nvars; ++i) {
					double val = 0;
					if ((rc = SF_vdata(i, j, &val))) return(rc);
					if (SF_is_missing(val)) {
						use = false;
					}
				}
				if (use == true) {
					if (row > 0 && IDS[row] != IDS[row - 1]) {
						++group;
					}
					int idx = 0;
					for (int k = 0; k < rparams3; ++k) {
						if ((rc = SF_vstore(varnum + idx, j, thet3[(N_g * k) + group]))) return(rc);
						++idx;
					}
					for (int k = 0; k < rvparams3; ++k) {
						if ((rc = SF_vstore(varnum + idx, j, pvar3[(N_g * k) + group]))) return(rc);
						++idx;
					}
					//if ((rc = SF_vstore(varnum, j, thet3[group]))) return(rc);
					//if ((rc = SF_vstore(varnum + 1, j, thet3[group + N_g]))) return(rc);
					//if ((rc = SF_vstore(varnum + 2, j, pvar3[group]))) return(rc);
					//if ((rc = SF_vstore(varnum + 3, j, pvar3[group + N_g]))) return(rc);
					//if ((rc = SF_vstore(varnum + 4, j, pvar3[group + (N_g * 2)]))) return(rc);
					++row;
				}
			}
		}
		//varnum += 5;
		varnum += rparams3 + rvparams3;
	}

	if (store_resi_1 == 1) {
		int row = 0;
		for (int j = SF_in1(); j <= SF_in2(); ++j) {
			if SF_ifobs(j) {
				bool use = true;
				for (int i = 1; i <= nvars; ++i) {
					double val = 0;
					if ((rc = SF_vdata(i, j, &val))) return(rc);
					if (SF_is_missing(val)) {
						use = false;
					}
				}
				if (use == true) {
					if ((rc = SF_vstore(varnum, j, zerr1[row]))) return(rc);
					++row;
				}
			}
		}
		++varnum;
	}

	if (store_resi_2 == 1) {
		int row = 0;
		for (int j = SF_in1(); j <= SF_in2(); ++j) {
			if SF_ifobs(j) {
				bool use = true;
				for (int i = 1; i <= nvars; ++i) {
					double val = 0;
					if ((rc = SF_vdata(i, j, &val))) return(rc);
					if (SF_is_missing(val)) {
						use = false;
					}
				}
				if (use == true) {
					if ((rc = SF_vstore(varnum, j, zerr2[row]))) return(rc);
					++row;
				}
			}
		}
		++varnum;
	}

	if (store_resi_3 == 1) {
		int row = 0;
		for (int j = SF_in1(); j <= SF_in2(); ++j) {
			if SF_ifobs(j) {
				bool use = true;
				for (int i = 1; i <= nvars; ++i) {
					double val = 0;
					if ((rc = SF_vdata(i, j, &val))) return(rc);
					if (SF_is_missing(val)) {
						use = false;
					}
				}
				if (use == true) {
					if ((rc = SF_vstore(varnum, j, zerr3[row]))) return(rc);
					++row;
				}
			}
		}
		++varnum;
	}

	return(0) ;
}

